////
TODO: explain how this relates to page versions
////
= Component Versions and Branches

Versions of a xref:component-structure.adoc[documentation component] are stored in branches in a version control repository such as git.
The name of the branch itself doesn't matter.
It's the version property in the component descriptor that determines the name of the version for the component.

== Branches as versions

Like with software, we use branches to store different versions of the documentation.
Branches are ideally suited for managing multiple versions of the same content.

If we didn't use branches to specify versions, but instead used folders with trailing version numbers, all stuffed in a single branch, then we'd have to explicitly duplicate all the files in a documentation component for each version.
And we'd have no easy way to compare, manage, and merge different instances of a document.

Branches handle all this for us.
Creating a new branch is extremely cheap.
You simply create a new branch from an existing reference in the repository, and the repository only stores what's changed since that branch point.

== Setting the version for a branch

To set the version of documentation stored in a particular branch, you specify the xref:component-descriptor.adoc#version-key[version in the component descriptor]:

[source,yaml]
----
name: versioned-component
version: '2.1'
title: Versioned Component
----

This component descriptor communicates that the files taken from this branch contribute to version 2.1 of the versioned-component component.
The name of the branch could be v2.1 or v2.1-beta.
It doesn't matter.

This is the only file you have to update when creating a new branch.
All the page references for that component should be relative to the version, so you shouldn't need to update any links.
The next time you run Antora on the repository, you'll see a new version in the component explorer drawer.

You may need to add the xref:playbook:configure-content-sources.adoc#branches[branch to your playbook file].
Keep in mind that content sources are filtered by branch name, not by the version they contain.
That's because a single version can be spread out across multiple branches, or even multiple repositories.

== How Antora sorts versions

Antora sorts the versions of a component.
To attain the order you want, you need to understand Antora's sorting rules.
Antora uses these rules:

* Filter out prerelease versions (unless the list only has prerelease versions).
* Drop the leading "`v`".
* Sort versions that start with a digit in descending order.
 ** Follow semantic versioning order as defined on https://semver.org.
* Sort the remaining versions alphabetically. Promote the group to the top.
* Move the version "`master`" to the top.

The lastest version is the one at the top.

Example:

----
master
el-capitan
v3.10
v3.9
v2.0
----

When Antora displays the versions, it also applies this sorting order.

== Versionless components

It's possible to make a component versionless.
A [.term]*versionless component* is one that has a single component version, but where the URL of its files do not contain a version segment in the published site.

To make a versionless component, you still need to use a branch.
However, you'll only use one.
Put all the files for the component in the master branch or a branch name of your choosing.
Then, assign the reserved word `master` to the component's version property in the component descriptor:

.antora.yml for a versionless component
[source,yaml]
----
name: tutorials
version: master
title: Tutorials
----

When the name of a component version is `master`, Antora removes the version segment from the URL (and output path) of all publishable files in that component version.
For example, a page in the versionless component shown above might have a URL like `/tutorials/build-a-restful-web-service.html`.
However, the version name can still be referenced in resource IDs for the purpose of making references (e.g., `master@tutorials::build-a-restful-web-service.adoc`).

If the version property has any value other than `master` (e.g., latest), that value will appear in the URL (and output path) of the files.

You can also make a hybrid component which has both regular component versions and a versionless component version.
//They don't even have to contain the same files.
